package agh.tests;

import agh.qa.PeselValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselValidatorTests {
    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"44051401359", true},
                {"4405140135", false},
                {"440514013597", false},
                {"4405140135!", false},
                {"44131401359", false},
                {"44023001359", false},
                {"20033001359", false},
                {"19022901359", false},
                {"20312251359", false},
                {"20011551359", false},
                {"20023001359", false},
                {"80851401359", false},
                {"21450201359", true},
                {"22650301359", true},
                {"16022601369", false},
                {"17222542174", false},

        };
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPesel(String pesel, boolean shouldBeValid){
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
    }
}
