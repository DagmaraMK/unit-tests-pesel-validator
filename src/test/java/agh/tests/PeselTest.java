package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.ParseException;

import static org.testng.Assert.*;

public class PeselTest {

    @Test
    public void testGetIndexWhenGreaterLenght () throws ParseException{
        Pesel toTest = PeselParser.Parse("92122535633");
        int actualIndex = toTest.getDigit (13);
        Assert.assertEquals(actualIndex, -1);

    }
    @Test
    public void testGetLessThanZero () throws ParseException{
        Pesel toTest= PeselParser.Parse("92111403994");
        int actualIndex= toTest.getDigit(-1);
        Assert.assertEquals(actualIndex, -1);
    }
}