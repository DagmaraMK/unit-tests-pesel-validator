package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselExtractor;
import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.time.LocalDate;

public class PeselExtractorTest {

    @Test
    public void testGetBirthDate() throws ParseException {
        Pesel peselToTest = PeselParser.Parse("92111403948");
        LocalDate expectedDate = LocalDate.of(1992, 11, 14);

        PeselExtractor peselExtractor = new PeselExtractor(peselToTest);
        LocalDate actualDate = peselExtractor.GetBirthDate();

        Assert.assertEquals(expectedDate, actualDate);
    }

    @Test
    public void testGetSexWhenReturnMale() throws ParseException {
        Pesel toTest = PeselParser.Parse("92122500744");
        PeselExtractor extractor = new PeselExtractor(toTest);
        String sex = extractor.GetSex();
        Assert.assertEquals("Male", sex);
    }

    @Test
    public void testGetSexWhenReturnFemale() throws ParseException {
        Pesel toTest = PeselParser.Parse("92122535633");
        PeselExtractor extractor = new PeselExtractor((toTest));
        String sex = extractor.GetSex();
        Assert.assertEquals("Female", sex);
    }


}
